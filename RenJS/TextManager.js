function TextManager(){

    this.show = function(text,title,colour){
        var t = RenJS.logicManager.parseVars(text);
        RenJS.gui.showText(t,title,colour,function(){
            console.log("Waiting for click")
            RenJS.waitForClick(RenJS.textManager.hide);
        });
    };

    this.hide = function(){
        RenJS.gui.hideText();
        RenJS.resolve();
    }

    this.say = function(name,text){
        var character = RenJS.chManager.characters[name];
        this.show(text,character.name,character.speechColour);
    }

    /**
     * 
     * @param {Object} param
     * This calls the gui point update function then resolves
     * and moves on to the next game action 
     */
    this.point = function(param){
        RenJS.gui.updatePoints(param[0].points, param[1].add)
        RenJS.resolve();
    }
}

