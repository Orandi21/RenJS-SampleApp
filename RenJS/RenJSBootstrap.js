
var game = new Phaser.Game(globalConfig.w, globalConfig.h, Phaser[globalConfig.mode], "RenJS");

var bootstrap = {

  preload: function () {
    game.load.image('splash',  globalConfig.splash.loadingScreen);
    if (globalConfig.splash.loadingBar) {
      game.load.image('loading',  globalConfig.splash.loadingBar.fullBar);
    }    
    game.load.script('preload',  'RenJS/Preload.js');
  },

  create: function () {
    game.state.add('preload', preload);
    game.state.start('preload');
  }

};

game.state.add('bootstrap', bootstrap);
game.state.start('bootstrap');