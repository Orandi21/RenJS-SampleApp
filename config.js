var globalConfig = {
  w:800,
  h:600,
  mode: "AUTO",
  splash: { //The "Loading" page for your game
    loadingScreen: "assets/gui/splash.png", //splash background
    loadingBar: {
      fullBar: "assets/gui/loadingbar.png",
      position: {x:111,y:462}
    }
  },
  fonts: "assets/gui/fonts.css",
  guiConfig: "Story/GUI.yaml",
  storySetup: "Story/Setup.yaml",
  //as many story text files as you want
  storyText: [
        "Story/YourStory.yaml"
    ],
}